#!/usr/bin/env node

const path = require('path');
const fs = require('fs');

const latex_renderer = require('./latex-renderer');
const downloader = require('./downloader');


const md = require('markdown-it')({
  html:         false,        // Enable HTML tags in source
  xhtmlOut:     false,        // Use '/' to close single tags (<br />).
                              // This is only for full CommonMark compatibility.
  breaks:       true,        // Convert '\n' in paragraphs into <br>
  langPrefix:   'language-',  // CSS language prefix for fenced blocks. Can be
                              // useful for external highlighters.
  linkify:      true,         // Autoconvert URL-like text to links

  // Enable some language-neutral replacement + quotes beautification
  typographer:  false,
  quotes: '„“‚‘'
});
md.inline.ruler.__rules__[md.inline.ruler.__find__('link')].fn = require('./rules-inline-link.js');
md.renderer = new latex_renderer();
md.renderer.downloader = new downloader();
md.renderer.DOWNLOAD_DIR = 'downloaded_images';

const meta = require('./meta.js');
md.use(meta);

md.use(require('./semantically-true-footnotes.js'));
//md.use(require('markdown-it-footnote'));
md.use(require('markdown-it-ins'));
md.use(require('markdown-it-sup'));
md.use(require('markdown-it-sup'));
md.use(require('markdown-it-mark'));
md.use(require('markdown-it-imsize'));

const Plugin = require('markdown-it-regexp');

// TOC
const tocPlugin = new Plugin(
  /^\s*\[TOC\]\s*$/i,
  (match, utils) => '\\tableofcontents'
);
md.use(tocPlugin);

// math
const mathCenteredPlugin = new Plugin(
  /(^|[^\\])\$\$(.*[^\\])\$\$/i,
  (match, utils) => match[0]
);
md.use(mathCenteredPlugin);

const mathInlinePlugin = new Plugin(
  /(^|[^\\])\$(.*[^\\])\$/i,
  (match, utils) => match[0]
);
md.use(mathInlinePlugin);

// <br>
const brPlugin = new Plugin(
  /<br\s*\/?>/i,
  (match, utils) => '\\\\\n'
);
md.use(brPlugin);

// ignore <details>
const ignoreDetailsPlugin = new Plugin(
  /^(<details>(<summary>(.*)<\/summary>)|<\/details>(<br\s?\/?>)?)?$/i,
  (match, utils) => match[3] ? md.renderInline(match[3], Object.assign({}, {no_document: true})) : ''
);
md.use(ignoreDetailsPlugin);


async function main() {
	let args = process.argv.slice(2)
	if (args.length > 0) {
		fs.readFile(args[0], {encoding: 'utf-8'}, (err, data) => {
			if (err) console.error('failed to read file '+args[0], err);
			
			out = md.render(data);
			
			function finish() {
				if (args.length > 1) {
					fs.writeFile(args[1], out, err => {
						if (err) throw err;
					});
				} else {
					console.log(out);
				}
			}
			if (md.renderer.downloader) {
				md.renderer.downloader.replace_renamed_files(out).then(o => {
					out = o;
					finish();
				});
			} else {
				finish();
			}
		});
	} else {
		let result = md.render('---\ntitle: FSR Nebensitzung 2019-11-27\ntags: sitzung, nebensitzung, protokoll\n---\n\n> **Datum: 27.11.2019**\n> Sitzungsleitung: \n> Protokollführung: \n> Sitzungsstart: 14:XX\n> Sitzungsende: 14:XX\n> \n> ***Anwesende***\n> *Gewählte:* Uta Lemke, Peter Nerlich\n> *Kooptierte:* Alexander Weiß, Daniel Heidelberger, Max Scholz, Max Winter, Justin Salzburg, Marvin Schütz, Niklas Merkelt, Eric Gesemann, Alexander Brugger, Johannes Thies\n> *Entschuldigte:* \n> *Gäste:* Tobias Hollstein, Anass Halime\n> *Fehlend:* \n> **Der FSR ist nicht beschlussfähig.**\n\n# TOP 8: Markdown-it rulezz!\n\n## bla\n\n>> Max Scholz erscheint zur Sitzung.\n\n> > Max Scholz verlässt die Sitzung.\n\n> > Max Scholz, Niklas Merkelt erscheinen zur Sitzung\n\n>> 14:32 Uhr Max Scholz, Niklas Merkelt verlassen die Sitzung.\n\nTest **bold** *it* [l**in**k](test.bla) drone.io.\n\n----\n#### Do we want more code? (9/2/0) (Dafür/Dagegen/Enthaltung)\n----\n\n> test1\n> test2\n> test3\n\n----\n\n- A\n- B\n  - 1\n  - 2\n- `Code`!\n\n```Python\nmore\ncode\n```\n');
		console.log(result, md.meta);
	}
}

main();
